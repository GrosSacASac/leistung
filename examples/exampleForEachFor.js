import { createPerformanceTest, runAll } from "../built/leistung.js";
import { defaultLogging } from "../source/defaultLogging.js";


const setup = (shared) => {
    shared.sideEffect = 0; // observable side effect
};

const iteration = 10000;
const sharedArray = Array.from({ length: iteration }, (_, i) => 2 * i);

const forEach = {
    name: 'forEach',
    code: (shared, finish) => {
        sharedArray.forEach(x => {
            shared.sideEffect -= shared.sideEffect + x;
        })
        finish();
    }
};

const forLoop = {
    name: 'forLoop',
    code: (shared, finish) => {
        const { length } = sharedArray;
        for (let i = 0; i < length; i += 1) {
            const x = sharedArray[i];
            shared.sideEffect -= shared.sideEffect + x;
        }
        finish();
    }
};

const testSuite = createPerformanceTest({
    tests: [forEach, forLoop],
    maxTime: 50,
    setup,
});
runAll(testSuite).then(defaultLogging);
