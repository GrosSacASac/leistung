import * as d from "../node_modules/dom99/built/dom99.es.js";

import { createPerformanceTest, runAll } from "../node_modules/leistung/built/leistung.js";
import { defaultLogging } from "../node_modules/leistung/source/defaultLogging.js";

const tests = [];
d.functions.addTest = () => {
    tests.push({});
    d.feed(`tests`, tests);
};

d.functions.start = async () => {
    const results = await runAll(createPerformanceTest({
        beforeAll: d.get(`beforeAll`),
        tests: tests.map((x, index) => {
            return {
                name: d.get(index, `name`),
                code: Function("shared", "finish", d.get(index, `code`)),
            };
        }),
        maxTime: 2,
    }));

    // todo get the result
    defaultLogging(results);
    d.feed(`results`, results.tests.x);
    // totalTime, totalRun, name, speed
    console.log(results);
};

d.start();

window.d = d;
