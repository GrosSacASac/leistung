# changelog

## 5.0.0

 * Requires import function support
 * nowReferential is no longer an option

## 2.0.0

 * nowReferential is an optional dependency
 * compatible with deno and the browser
 * format as ES module
 * exports createPerformanceTest, runAll
 * depends upon utilsac (deduplication of code)
 * add example
 * use ms unit everywhere
 * improved output

## 1.0.0

 * moved to a self contained project
 * renamed to leistung
 * published to npm