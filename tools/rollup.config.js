import resolve from 'rollup-plugin-node-resolve';


export default {
    input: `source/leistung.js`,
    output: {
        file: 'built/leistung.js',
        format: 'esm',
        sourcemap: false,
    },
    plugins: [
        resolve(),
    ]
};
