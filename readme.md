# leistung

## warning

The precision is not good, so fast operation have high variance results.

## import


```js
import { performance } from "node:perf_hooks"; // optional, only for node
// node12, rollup, webpack
import { createPerformanceTest, runAll } from "leistung";
// raw import for browser, deno
import { createPerformanceTest, runAll } from "./node_modules/leistung/built/leistung.js";
import { defaultLogging } from "./node_modules/leistung/source/defaultLogging.js";// optional
```

## usage

see ./examples/exampleForEachFor.js
