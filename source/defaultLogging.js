export { defaultLogging };

const defaultLogging = (results, logger = console) => {
    Object.values(results.tests).forEach(({ totalTime, totalRun, name, speed }) => {
        console.log(`\t* ${name} ran ${totalRun} times in ${totalTime} ms, at ${speed} operation/ms`);
    });
    const {
        best,
        bestTotalRun,
        bestSpeed,
    } = results;
    console.log(`\nBest: ${best} at ${bestSpeed} operation/ms`);
};
