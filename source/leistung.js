export { createPerformanceTest, runAll };

import { chainPromises } from "utilsac";
import { evalGlobal } from "utilsac/browserUtility.js";
import { isNodeJs } from "./isNodeJs.js";


const empty = () => { };

const createPerformanceTest = (options) => {
    const {
        beforeAll = empty,
        setup = empty,
        teardown = empty,
        tests,
        maxTime = 200,
    } = options;


    return {
        beforeAll,
        setup,
        teardown,
        tests,
        maxTime,
        nowReferential: undefined,
        results: undefined,
    };
};

const runAll = async (performanceTest) => {
    const { beforeAll, tests } = performanceTest;
    let performanceAlias;
    if (isNodeJs()) {
        const { performance } = await import("perf_hooks");
        performanceAlias = performance;
    } else {
        performanceAlias = performance;
    }
    try {
        performanceTest.nowReferential = performanceAlias.now.bind(performanceAlias);
    } catch (error) {
        console.warn(`could not use performance.now`);
        console.error(error);
    }
    if (typeof beforeAll === `string`) {
        await evalGlobal(beforeAll);
    } else {
        beforeAll();
    }
    const rawResults = await chainPromises(performanceTest.tests.map(test => {
        return runOne.bind(undefined, performanceTest, test);
    }))

    const results = { tests: {} };
    let best;
    let bestTotalRun;
    let bestSpeed = -1;
    rawResults.forEach(rawResult => {
        const { totalTime, totalRun, name } = rawResult;
        const speed = totalRun / totalTime;
        rawResult.speed = speed;
        results.tests[name] = rawResult;
        if (speed > bestSpeed) {
            bestSpeed = speed;
            best = name;
            bestTotalRun = totalRun;
        }
    });
    Object.assign(results, {
        best,
        bestTotalRun,
        bestSpeed,
    });
    performanceTest.results = results;
    return results;
};

const runOne = (performanceTest, test) => {
    const {
        setup,
        teardown,
        maxTime,
        nowReferential
    } = performanceTest;

    const {
        name,
        code
    } = test;

    let totalTime = 0;
    let lastStart = 0;
    let totalRun = 0;
    let nextTimeoutId; // not used yet

    const runOneChained = (whenFinished) => {
        nextTimeoutId = setTimeout(() => {
            const shared = {};
            setup(shared);
            lastStart = nowReferential();
            code(shared, () => {
                totalTime += nowReferential() - lastStart;
                totalRun += 1;
                teardown(shared);
                if (totalTime > maxTime) {
                    whenFinished({ totalTime, totalRun, name });
                } else {
                    runOneChained(whenFinished);
                }
            });
        }, 0);
    };

    return new Promise((resolve, reject) => {
        runOneChained(resolve);
    });
};
