export {
    isNodeJs,
};

const isNodeJs = () => {
    return Boolean(globalThis.Buffer);
};